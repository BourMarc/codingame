//données du jeu
const N = parseInt(readline());
const W = [];
for (let i = 0; i < N; i++) {
    W[i] = readline();
}
const lettres = readline();
//fin données du jeu

//liste des variables utilisées
let mots_possibles = [] ;
let solutions = mots_possibles;

//tableau de points 
const tab_score = [1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10];
//                 a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z

function suppr_lettre(mot, index) {
  let part1 = mot.substring(0, index);
  let part2 = mot.substring(index + 1, mot.length);
  return (part1 + part2);
 }

function calcul_score(mot) {
    let score = 0 ;
    for (let i = 0; i < mot.length ; i++) {
        let index = mot.charCodeAt(i) - 97;
        score += tab_score[ index ] ;
    }
    return score;
}

for (let i = 0; i < N; i++) { //on teste chaque mot index i du dictionnaire W
    let test = 0;
    let tmp_lettres = lettres;
    for (let j = 0; j < W[i].length ; j++) { 
        //on cherche chaque lettre index j du mot W dans la liste de lettres, 
        //si on trouve on supprime la lettre de la liste et on continue, si on ne trouve pas, mot suivant
        test = tmp_lettres.indexOf( W[i].charAt(j) ) ;
        if(test === -1) { break; }
        tmp_lettres = suppr_lettre(tmp_lettres, test) ;
    }
    if(test !== -1) { 
        mots_possibles.push({mot: W[i], score: calcul_score(W[i]), entry: i});
    }
}

//classement par score
mots_possibles.sort( function(a, b) { return b.score - a.score; });

//puis classement par ordre d'apparition dans le dictionnaire si nécessaire
if( mots_possibles.length > 1) {
    let cut = 0;
    for(let i = 1; i < mots_possibles.length ; i++) {
        cut++ ;
        if(mots_possibles[i].score !== mots_possibles[i-1].score ) { break; }  
    }
    solutions = mots_possibles.slice(0, cut);
}
solutions.sort( function(a, b) { return a.entry - b.entry; });

print( solutions[0].mot );